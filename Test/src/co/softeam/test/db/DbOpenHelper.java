package co.softeam.test.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbOpenHelper extends SQLiteOpenHelper {
	
	private static final String 	TAG 				= "DbOpenHelper";
	private static final String		DATABASE_NAME		= "db.db";
	private static final int 		DATABASE_VERSION 	= 1;
	private static DbOpenHelper mInstance;
    private static SQLiteDatabase db;
	
	private static final String[]	CREATE_SCRIPT 		= new String[] {
		 
    	"CREATE TABLE friends	    (_id 			int PRIMARY KEY, " +
    	    						"name	 		text," +
    	    						"soname			text," +
    	    						"nick		 	text, " +
    	    						"bdate		 	int, " +
    	    						"photo		 	text)",
    	
		
    };
    
    
	public DbOpenHelper(Context context) {
	    	super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Get default instance of the class to keep it a singleton
     *
     * @param context
     *            the application context
     */
    public static DbOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbOpenHelper(context);
        }
        return mInstance;
    }
    
    /**
    * Returns a writable database instance in order not to open and close many
    * SQLiteDatabase objects simultaneously
    *
    * @return a writable instance to SQLiteDatabase
    */
   public SQLiteDatabase getDb() {
       if ((db == null) || (!db.isOpen())) {
           db = this.getWritableDatabase();
       }

       return db;
   }
   
   @Override
   public void close() {
       super.close();
       if (db != null) {
           db.close();
           db = null;
       }
       
       mInstance = null;
   }

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (String sql_line : CREATE_SCRIPT) {
			try {
				db.execSQL(sql_line);
			} catch (Exception e) {
				String err = (e.getMessage()==null)?"Unknown Error":e.getMessage();
				Log.e(TAG, err);
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	


}
