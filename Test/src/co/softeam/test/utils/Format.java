package co.softeam.test.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Format {

	public static Date time(String date1) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd");
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	        return dateFormat.parse(date1);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static Date getTimeInUTCZone(Date currentDate) {
	       //TimeZone tz = TimeZone.getTimeZone(timeZoneId);
	       Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	       mbCal.setTimeInMillis(currentDate.getTime());
	       Calendar cal = Calendar.getInstance();
	       cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
	       cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
	       cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
	       cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
	       cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
	       cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
	       cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
	       return cal.getTime();
	   }
	

}
