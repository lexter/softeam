package co.softeam.test.utils;

import java.util.Calendar;

public class VkDateParser {

	public int year;
	public long bdate;
	
	public VkDateParser(String date) {
		try {
			if (date.equals("0")) return;
			int j=0;
			String day="",month="",year="";
			for (int i=0;i<date.length();i++) {
				if (date.charAt(i)=='.') {
					j++;
					continue;
				}
				if (j==0) {
					day+=date.charAt(i);
				}
				if (j==1) {
					month+=date.charAt(i);
				}
				if (j==2) {
					year+=date.charAt(i);
				}
			}
			if (day.length()<2) {
				day="0"+day;
			}
			if (month.length()<2) {
				month="0"+month;
			}
			bdate = Format.getTimeInUTCZone(Format.time(month+"."+day)).getTime();
			this.year = 0;
			if (!year.equals("")) {
				Calendar calendar = Calendar.getInstance();
				this.year = calendar.get(Calendar.YEAR)-Integer.parseInt(year);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
