package co.softeam.test.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Setting {

	static SharedPreferences sPref;

	public static void setParam(String param, String value, Context context) {
		sPref = PreferenceManager.getDefaultSharedPreferences(context);
		Editor ed = sPref.edit();
		ed.putString(param, value);
		ed.commit();
	}

	public static String getParam(String param, Context context) {
		sPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sPref.getString(param, "");
	}

}
