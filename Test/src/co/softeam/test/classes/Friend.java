package co.softeam.test.classes;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import co.softeam.test.db.DbOpenHelper;
import co.softeam.test.utils.HttpCommand;
import co.softeam.test.utils.Setting;
import co.softeam.test.utils.VkDateParser;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Friend {
	
	public int _id;
	public String name;
	public String soname;
	public String nick;
	public String photo;
	public long bdate;
	
	public Friend(Cursor cursor, Context context) {
		_id = cursor.getInt(cursor.getColumnIndex("_id"));
		name = cursor.getString(cursor.getColumnIndex("name"));
		soname = cursor.getString(cursor.getColumnIndex("soname"));
		nick = cursor.getString(cursor.getColumnIndex("nick"));
		bdate = cursor.getLong(cursor.getColumnIndex("bdate"));
		photo = cursor.getString(cursor.getColumnIndex("photo"));
	}
	
	public Friend(JSONObject object,Context context) {
		_id = object.optInt("uid");
		name = object.optString("first_name").toString();
		soname = object.optString("last_name").toString();
		nick = object.optString("nickname").toString();
		photo = object.optString("photo_medium").toString();
		VkDateParser dateParser = new VkDateParser(object.optString("bdate"));
		bdate = dateParser.bdate;
	}
	
	public Friend getFriendById(SQLiteDatabase db,Context context,int _id) {
		Cursor cursor = null;
		cursor = db.rawQuery("SELECT * FROM friends WHERE _id="+_id,null);
		if (cursor.moveToFirst()) {
			try {
				Friend friend = new Friend(cursor,context);
				return friend;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
	public static ArrayList<Friend> getAllFriends(SQLiteDatabase db,Context context) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR,0);
		
		Cursor cursor = null;
		cursor = db.rawQuery("SELECT * FROM friends WHERE bdate>0 AND bdate ORDER BY bdate ASC",null);
		ArrayList<Friend> friends = new ArrayList<Friend>();
		while (cursor.moveToNext()) {
			try {
				Friend friend = new Friend(cursor,context);
				friends.add(friend);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return friends;
	}
	
	public void save(SQLiteDatabase db) {
		try {
			ContentValues insertValues = new ContentValues();
			if (_id!=0) insertValues.put("_id", _id);
			insertValues.put("name", name);
			insertValues.put("soname", soname);
			insertValues.put("nick", nick);
			insertValues.put("bdate", bdate);
			insertValues.put("photo", photo);
			_id = (int) db.insert("friends", null, insertValues);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean clear(SQLiteDatabase db) {
		
		try {
			db.execSQL("DELETE FROM friends");
			return true;
		} catch (Exception e) {
			Log.e("MESSAGE ERROR","REMOVE MESSAGE DB ERROR. message: "+e.getMessage());
			return false;
		}
	}
	
	
	public static boolean updateFriends(Context context) {
		try {
			HttpCommand command = new HttpCommand("https://api.vk.com/method/friends.get?uid="+Setting.getParam("vid", context)+"&fields=uid,bdate,first_name,last_name,nickname,screen_name,photo,photo_medium,photo_big&access_token=" + Setting.getParam("vtoken" , context), true);
			String result = command.executeForString();
			clear(DbOpenHelper.getInstance(context).getDb());
			JSONObject jObject = new JSONObject(result);
			JSONArray menuitemArray = jObject.getJSONArray("response");
			System.out.println(menuitemArray.length());
			for (int i=0;i<menuitemArray.length();i++) {
				Friend friend = new Friend(menuitemArray.getJSONObject(i),context);
				friend.save(DbOpenHelper.getInstance(context).getDb());
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
