package co.softeam.test;

import java.util.ArrayList;

import co.softeam.test.adapters.FriendListAdapter;
import co.softeam.test.classes.Friend;
import co.softeam.test.db.DbOpenHelper;
import co.softeam.test.socialapi.VkLoginActivity;
import co.softeam.test.utils.Setting;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {

	FriendListAdapter adapter;
	SwipeRefreshLayout swipeRefreshLayout;
	Handler handler;
	ListView listView;
	LinearLayout header;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        handler = new Handler(); 

        initView();
        
    }

    void initView() {
    	
    	listView = (ListView) findViewById(R.id.friends_listview);
    	
    	ArrayList<Friend> friends = new ArrayList<Friend>();
    	adapter = new FriendListAdapter(getApplicationContext(),R.layout.item_friends_list,friends);
    	View view = View.inflate(getApplicationContext(), R.layout.item_head, null);
    	listView.addHeaderView(view);
    	listView.setAdapter(adapter);
	
    	header =  (LinearLayout) view.findViewById(R.id.layoutHead);
		
    	Button button = (Button) view.findViewById(R.id.buttonLogin);
    	button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,VkLoginActivity.class);
				startActivityForResult(intent, 666);
				return;
			}
		});
    	
    	swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
    	swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
			
			@Override
			public void onRefresh() {
				updateFriendsList();
			}
		});
    	
    	if (Setting.getParam("vtoken", getApplicationContext()).equals("")) {
    		showHeaderForAuthorize(true);
    	} else {
    		showHeaderForAuthorize(false);
    		adapter.clear();
			adapter.addAll(Friend.getAllFriends(DbOpenHelper.getInstance(getApplicationContext()).getDb(),getApplicationContext()));
			adapter.notifyDataSetChanged();
    	}
		
    }
    
    void showHeaderForAuthorize(boolean show) {
    	if (show) header.setVisibility(View.VISIBLE);
    	else header.setVisibility(View.GONE);
    }
    
    public void updateFriendsList() {
    	swipeRefreshLayout.setRefreshing(true);
    	if (Setting.getParam("vtoken", getApplicationContext()).equals("")) {
    		showHeaderForAuthorize(true);
    		swipeRefreshLayout.setRefreshing(false);
    		return;
    	}
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
				Friend.updateFriends(getApplicationContext());
				handler.post(new Runnable() {
					
					@Override
					public void run() {
						showHeaderForAuthorize(false);
						adapter.clear();
						adapter.addAll(Friend.getAllFriends(DbOpenHelper.getInstance(getApplicationContext()).getDb(),getApplicationContext()));
						adapter.notifyDataSetChanged();
						swipeRefreshLayout.setRefreshing(false);
					}
				});
				
			}
		}).start();
    }
    
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode==666 && resultCode == RESULT_OK) {
			Setting.setParam("vtoken", data.getExtras().getString("token"), getApplicationContext());
			Setting.setParam("vid", String.valueOf(data.getExtras().getLong("user_id")), getApplicationContext());
			updateFriendsList();
			showHeaderForAuthorize(false);
		} else {
			showHeaderForAuthorize(true);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_update) {
        	if (swipeRefreshLayout.isRefreshing()) return false;
        	updateFriendsList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
