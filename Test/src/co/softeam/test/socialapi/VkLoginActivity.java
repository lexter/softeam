package co.softeam.test.socialapi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.softeam.test.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class VkLoginActivity extends Activity {

	private static final String TAG = "login";

	Context context;
	private final String APP_ID = "4415705";
	private final String SCOPE = "offline,messages,friends,wall,photos";
	private final String REDIRECT_URI = "https://oauth.vk.com/blank.html";
	private final String DISPLAY = "touch";

	WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_vklogin);

		context = this;
		
		webview = (WebView) findViewById(R.id.webViewAutintification);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.clearCache(true);

		webview.setWebViewClient(new VKWebViewClient());

		CookieSyncManager.createInstance(this);

		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();

		webview.loadUrl("https://oauth.vk.com/authorize?" + "client_id="
				+ APP_ID + "&" + "scope=" + SCOPE + "&" + "redirect_uri="
				+ REDIRECT_URI + "&" + "display=" + DISPLAY + "&"
				+ "response_type=token");

	}

	class VKWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			parseUrl(url);
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
		}
		
		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Toast.makeText(context, context.getText(R.string.error_connect), Toast.LENGTH_SHORT).show();
			finish();
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
	}

	private void parseUrl(String url) {
		try {
			if (url == null)
				return;
			Log.i(TAG, "url=" + url);
			if (url.toLowerCase().startsWith(REDIRECT_URI.toLowerCase())) {
				if (!url.contains("error=")) {
					String[] auth = parseRedirectUrl(url);
					Intent intent = new Intent();
					intent.putExtra("token", auth[0]);
					intent.putExtra("user_id", Long.parseLong(auth[1]));
					setResult(Activity.RESULT_OK, intent);
				}
				finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String extractPattern(String string, String pattern) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(string);
		if (!m.find())
			return null;
		return m.toMatchResult().group(1);
	}

	public static String[] parseRedirectUrl(String url) throws Exception {
		String access_token = extractPattern(url, "access_token=(.*?)&");
		Log.i(TAG, "access_token=" + access_token);
		String user_id = extractPattern(url, "user_id=(\\d*)");
		Log.i(TAG, "user_id=" + user_id);
		if (user_id == null || user_id.length() == 0 || access_token == null || access_token.length() == 0)
			throw new Exception("Failed to parse redirect url " + url);
		return new String[] { access_token, user_id };
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.activity_vklogin, menu);
		return true;
	}
}