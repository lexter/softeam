package co.softeam.test.adapters;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.softeam.test.R;
import co.softeam.test.classes.Friend;
import co.softeam.test.utils.BitmapHelper;
import co.softeam.test.utils.RoundedImageView;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FriendListAdapter extends ArrayAdapter<Friend> {

		ExecutorService service = Executors.newCachedThreadPool();
	
		public FriendListAdapter(Context context, int resource, List<Friend> films) {
			super(context, resource, films);
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view!=null) {
				if (view.findViewById(R.id.textViewName).getTag().equals(getItem(position).name+" "+getItem(position).soname)) return view;
			}
			
			view = LayoutInflater.from(getContext()).inflate(R.layout.item_friends_list, parent, false);
			final TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
			textViewName.setText(getItem(position).name+" "+getItem(position).soname);
			textViewName.setTag(getItem(position).name+" "+getItem(position).soname);
			
			Date bdate = new Date(getItem(position).bdate);
			final TextView textViewBdate = (TextView) view.findViewById(R.id.textViewBdate);
			textViewBdate.setText(String.format("%te", bdate)+" "+String.format(Locale.getDefault(),"%tB",bdate));
			
			final RoundedImageView imageView = (RoundedImageView) view.findViewById(R.id.imageViewPhoto);
			service.submit(new Runnable() {
				
				@Override
				public void run() {
					final Bitmap bitmap = BitmapHelper.getPicture(getContext(), getItem(position).photo);
					imageView.post(new Runnable() {
						
						@Override
						public void run() {
							if (bitmap!=null) imageView.setImageBitmap(bitmap);
						}
					});
				}
			});
			
			return view;
		}
		
	}
